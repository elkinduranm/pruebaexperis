import { element } from 'protractor';
import { ServiceDepartementosService } from './servicios/service-departementos.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'pruebaFrontend';
  departamentos: any;
  consulta: object;
  ciudades: any;
  ciudad: string;
  departamento: string;
  nombre: string;
  correo: string;
  validaNombre = '';
  validaCorreo = '';
  respuesta = 'Procesando...';
  constructor(private seviceDepartamento: ServiceDepartementosService){

  }
  ngOnInit(): void {
    this.seviceDepartamento.getDepartamentos().subscribe(resultado => {
      console.log(resultado);
      this.departamentos = Object.keys(resultado);
      this.consulta = resultado;
      this.departamento = this.departamentos[0];
      this.ciudades = this.consulta[this.departamentos[0]];
      this.ciudad = this.ciudades[0];
    });
  }

  onChange($event): void{
    console.log($event);
    this.ciudades = this.consulta[$event];
    this.departamento = $event;
    this.ciudad = this.ciudades[0];
  }

  onChangeCiudad($evento): void{
    this.ciudad = $evento;
  }

  enviarClick(): void{
    let validad = 1;
    if (this.nombre == null) {
      this.validaNombre = 'El nombre es requerido';
      validad = validad * 0;
    }
    if (this.correo == null) {
      this.validaCorreo = 'El correo es requerido';
      validad = validad * 0;
    }
    else if (!this.correo.indexOf('@') || this.correo.length <= 1)
    {
      this.validaCorreo = 'El correo no es valido';
      validad = validad * 0;
    }
    if (validad === 0) { return; }
    const datos = new FormData();
    datos.append('id', '0');
    datos.append('name', this.nombre);
    datos.append('email', this.correo);
    datos.append('state', this.departamento);
    datos.append('city', this.ciudad);
    this.seviceDepartamento.Registrar(datos).subscribe(resultado => {
      if (resultado != null){
        this.respuesta = 'Contacto Registrado';
      }
      console.log(resultado);
    });
    this.limpiar();
  }

  limpiar(): void{
    this.nombre = '';
    this.correo = '';
    this.validaNombre = '';
    this.validaCorreo = '';
    this.respuesta = 'Procesando...';
  }

}
