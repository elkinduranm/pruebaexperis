import { TestBed } from '@angular/core/testing';

import { ServiceDepartementosService } from './service-departementos.service';

describe('ServiceDepartementosService', () => {
  let service: ServiceDepartementosService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ServiceDepartementosService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
