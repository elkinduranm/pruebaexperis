import { Injectable } from '@angular/core';
import {HttpClient, HttpParams, HttpHeaders} from '@angular/common/http';
import { Observable, BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ServiceDepartementosService {
  private apiDepartamentos = '/api/colombia';
  private apiContactos = '/api/Contacts';
  constructor(private httpClient: HttpClient) { }

  getDepartamentos(): Observable<any>{
    const header = new HttpHeaders()
    .set('Content-Type', 'application/json');
    return this.httpClient.get(this.apiDepartamentos);
  }

  Registrar(datos: FormData): Observable<any>{
    return this.httpClient.post(this.apiContactos, datos);
  }

}
