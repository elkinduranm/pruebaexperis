﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using pruebaBackend.Models;

namespace pruebaBackend.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ContactsController : ControllerBase
    {
        private readonly PruebaContext _context;

        public ContactsController(PruebaContext context)
        {
            _context = context;
        }

        // GET: api/Contacts
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ContactsEntity>>> Getcontacts()
        {
            return await _context.contacts.ToListAsync();
        }

        // GET: api/Contacts/5
        [HttpGet("{id}")]
        public async Task<ActionResult<ContactsEntity>> GetContactsEntity(int id)
        {
            var contactsEntity = await _context.contacts.FindAsync(id);

            if (contactsEntity == null)
            {
                return NotFound();
            }

            return contactsEntity;
        }

        // PUT: api/Contacts/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutContactsEntity(int id, ContactsEntity contactsEntity)
        {
            if (id != contactsEntity.id)
            {
                return BadRequest();
            }

            _context.Entry(contactsEntity).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ContactsEntityExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Contacts
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<ContactsEntity>> PostContactsEntity([FromForm]ContactsEntity contactsEntity)
        {
            _context.contacts.Add(contactsEntity);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetContactsEntity", new { id = contactsEntity.id }, contactsEntity);
        }

        // DELETE: api/Contacts/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<ContactsEntity>> DeleteContactsEntity(int id)
        {
            var contactsEntity = await _context.contacts.FindAsync(id);
            if (contactsEntity == null)
            {
                return NotFound();
            }

            _context.contacts.Remove(contactsEntity);
            await _context.SaveChangesAsync();

            return contactsEntity;
        }

        private bool ContactsEntityExists(int id)
        {
            return _context.contacts.Any(e => e.id == id);
        }
    }
}
