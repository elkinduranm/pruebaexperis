﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace pruebaBackend.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class colombiaController : ControllerBase
    {
        // GET: api/<colombiaController>
        //[EnableCors]
        [HttpGet]
        public async Task<string> Get()
        {
            return await departamentos();
        }

        // GET api/<colombiaController>/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        public async Task<string> departamentos()
        {
            try
            {
                HttpClient client;
                client = new HttpClient();
                client.MaxResponseContentBufferSize = 256000;
                string url = "https://sigma-studios.s3-us-west-2.amazonaws.com/test/colombia.json";
                var response = await client.GetAsync(url);

                if (response.IsSuccessStatusCode)
                {
                    string content = await response.Content.ReadAsStringAsync();

                    return content;
                }

                return string.Empty;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }


        // POST api/<colombiaController>
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT api/<colombiaController>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/<colombiaController>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
